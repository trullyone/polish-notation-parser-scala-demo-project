import scala.collection.immutable.Nil
import scala.util.{Failure, Success, Try}

//Unwrap const extension methods
import ConstExtension._

abstract sealed class Expression
{
  def eval() : Int
}
case class Operator(operand1: Expression, operand2: Expression, operation: Operation) extends Expression
{
  def eval() : Int = operation match {
    case Sub() => operand1.eval() - operand2.eval()
    case Sum() => operand1.eval() + operand2.eval()
    case Mul() => operand1.eval() * operand2.eval()
    case Div() => operand1.eval() / operand2.eval()
  }
}
case class Const(value: Int) extends Expression
{
  def eval() : Int = value
}

sealed trait Operation
case class Mul() extends Operation
case class Sum() extends Operation
case class Sub() extends Operation
case class Div() extends Operation

object ConstExtension
{
  def asConst(x: String): Option[Const] = Try(x.toInt) match
  {
    case Success( value ) => Some(Const(value))
    case Failure(_) => None
  }
}

object PolishNotation
{
  def parse(tokens: List[String]): Option[Expression] ={
    parseExpression(tokens) match {
      case Some((exp, Nil)) => Some(exp)
      case _ => None
    }
  }

  private def parseExpression(tokens: List[String]): Option[(Expression, List[String])] ={
     tokens match {
        case "-"::tail => parseOperands(Sub(), tail)
        case "+"::tail => parseOperands(Sum(), tail)
        case "*"::tail => parseOperands(Mul(), tail)
        case "/"::tail => parseOperands(Div(), tail)
        case _ => parseConst(tokens)
    }
  }

  private def parseOperands(op: Operation, tokens: List[String]): Option[(Expression, List[String])] ={
    for {
      (expr, tail) <- parseExpression(tokens)
      (expr2, tail2) <- parseExpression(tail)
    } yield (Operator(expr, expr2, op), tail2)
  }

  private def parseConst(tokens: List[String]): Option[(Expression, List[String])] ={
    tokens match {
        case x::tail =>
          asConst(x) match {
            case Some(expr) => Some(expr, tail)
            case _ => None
          }
        case _ => None
    }
  }
}

object ReversePolishNotation
{
  def parse(tokens: List[String]): Option[Expression] ={
    parseExpression(tokens, Nil) match {
      case (Nil, x::Nil) => Some(x)
      case _ => None
    }
  }

  private def parseExpression(tokens: List[String], expressionsStack: List[Expression]): (List[String], List[Expression]) = {
    tokens match {
      case Nil => (Nil, expressionsStack)
      case "-"::tail => parseOperands(Sub(), tail, expressionsStack)
      case "+"::tail => parseOperands(Sum(), tail, expressionsStack)
      case "*"::tail => parseOperands(Mul(), tail, expressionsStack)
      case "/"::tail => parseOperands(Div(), tail, expressionsStack)
      case _ => parseConst(tokens, expressionsStack)
    }
  }

  private def parseOperands(op: Operation, tokens: List[String], expressionsStack: List[Expression]): (List[String], List[Expression]) ={
    expressionsStack match {
      case op1::op2::tail => parseExpression(tokens, Operator(op2, op1, op)::tail)
      case _ => (tokens, Nil)
    }
  }

  private def parseConst(tokens: List[String], expressionsStack: List[Expression]): (List[String], List[Expression]) ={
    tokens match {
      case x::tail => asConst(x) match {
        case Some ( const ) => parseExpression(tail, const::expressionsStack)
        case None => (tokens, Nil)
      }
      case _ => (tokens, Nil)
    }
  }
}

