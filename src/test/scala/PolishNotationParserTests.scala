import org.scalatest.{FlatSpec, Matchers}

class PolishNotationSpec extends FlatSpec with Matchers
{
  "− × ÷ 15 − 7 + 1 1 3 + 2 + 1 1" should "evaluate to 5" in
  {
    val input = List("-", "*", "/", "15", "-", "7", "+", "1", "1", "3", "+", "2", "+", "1", "1")
    val result = PolishNotation.parse(input) match {
      case Some(expr) => expr.eval()
    }
    assert(result == 5)
  }

  "5" should "evaluate to 5" in
    {
      val input = List("5")
      val result = PolishNotation.parse(input) match {
        case Some(expr) => expr.eval()
      }
      assert(result == 5)
    }

  "+ 3 2" should "evaluate to 5" in
    {
      val input = List("5")
      val result = PolishNotation.parse(input) match {
        case Some(expr) => expr.eval()
      }
      assert(result == 5)
    }

  "- 3 2" should "evaluate to 1" in
    {
      val input = List("-", "3", "2")
      val result = PolishNotation.parse(input) match {
        case Some(expr) => expr.eval()
      }
      assert(result == 1)
    }

  "- 2 3" should "evaluate to -1" in
    {
      val input = List("-", "2", "3")
      val result = PolishNotation.parse(input) match {
        case Some(expr) => expr.eval()
      }
      assert(result == -1)
    }

  "/ 6 2" should "evaluate to 3" in
    {
      val input = List("/", "6", "2")
      val result = PolishNotation.parse(input) match {
        case Some(expr) => expr.eval()
      }
      assert(result == 3)
    }
}