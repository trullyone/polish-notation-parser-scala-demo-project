import org.scalatest.{FlatSpec, Matchers}

class ReversePolishNotationSpec extends FlatSpec with Matchers
{
  "15 7 1 1 + − ÷ 3 × 2 1 1 + + −" should "evaluate to 5" in
  {
    val input = List("15", "7", "1", "1", "+", "-", "/", "3", "*", "2", "1", "1", "+", "+", "-")
    val result = ReversePolishNotation.parse(input) match {
      case Some(expr) => expr.eval()
    }
    assert(result == 5)
  }
}